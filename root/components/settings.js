function Settings(scene, params, Utils) {
    const eventManager = scene.getService("getEventManager");
    const parent = params.parent
    const keys = params.keys
    const height = params.itemHeight
    // const options = params.options
    const optionHeight = height * 0.7
    const fullScreenMarginLeft = scene.w * 0.035
    this.indexColumn = [0, 0, 0, 0];
    let idColumn = ""
    let idText = ""
    let nextColumn = true

    this.currentIndex = 0;
    this.currentColumnIndex = 0;
    this.previousColumnIndex = 0;
    this.previousIndex = 0;
    const selectedColor = 0x3e81baff
    const defaultColor = 0x363636ff
    const selectedTextColor = 0xedb928ff
    const defaultTextColor = 0xfefefeff
    let currentSetting = null
    let previousSetting = null
    let avStatusObjectMap = {}
    avStatusObjectMap["Device.Services.STBService.1.Components.HDMI.1.ResolutionValue"] = "TODO";
    avStatusObjectMap["Device.Services.STBService.1.Components.VideoOutput.1.DisplayFormat"] = "TODO";
    avStatusObjectMap["Device.Services.STBService.1.Components.HDMI.1.DisplayDevice.SupportedResolutions"] = "TODO";
    avStatusObjectMap["Device.Services.STBService.1.Components.VideoDecoder.1.ContentAspectRatio"] = "TODO";
    avStatusObjectMap["Device.Services.STBService.1.Components.VideoOutput.1.HDCP"] = "TODO";
    avStatusObjectMap["Device.Services.STBService.1.Components.AudioOutput.1.X_COMCAST-COM_AudioStereoMode"] = "TODO";
    avStatusObjectMap["Device.Services.STBService.1.Components.HDMI.1.DisplayDevice.PreferredResolution"] = "TODO";

    if(Utils.modelName.indexOf("PX051") !== -1)
    {
        avStatusObjectMap["Device.DeviceInfo.X_RDKCENTRAL-COM_xBlueTooth.Enabled"] = "TODO";
        avStatusObjectMap["Device.DeviceInfo.X_RDKCENTRAL-COM_xBlueTooth.DeviceInfo.Profile"] = "TODO";
        avStatusObjectMap["Device.DeviceInfo.X_RDKCENTRAL-COM_xBlueTooth.DeviceInfo.Manufacturer"] = "TODO";
    }

    let optionsSetting = [
        {
            name: "General",
            children: [
                {
                    name: "Video",
                    children: [
                        {
                            name: "Aspect Ratio",
                            children: [
                                {
                                    name: avStatusObjectMap["Device.Services.STBService.1.Components.VideoDecoder.1.ContentAspectRatio"]
                                }
                            ]
                        },
                        {
                            name: "Resolution",
                            children: [
                                {
                                    name: avStatusObjectMap["Device.Services.STBService.1.Components.HDMI.1.ResolutionValue"]
                                }
                            ]
                        }
                    ]
                },
                {
                    name: "Audio Mode",
                    children: [
                        {
                            name: avStatusObjectMap["Device.Services.STBService.1.Components.AudioOutput.1.X_COMCAST-COM_AudioStereoMode"]
                        }
                    ]
                },
                {
                    name: "Help"
                }
            ]
        },
        {
            name: "System",
            children: [
                {
                    name: "Test System 1"
                },
                {
                    name: "Test System 2"
                }
            ]
        }
    ]

    // const fontRegular = scene.create({
    //     t: "fontResource", url: '/usr/share/fonts/DejaVuSans.ttf'
    // });
    const settingViewX = parent.x + 250 - fullScreenMarginLeft
    const settingViewY = parent.y
    const settingViewW = parent.w
    const settingViewH = parent.h
    this.container = scene.create({
        t: "rect", parent, w: settingViewW, h: settingViewH, fillColor: 0x00000000, x: settingViewX, y: 0
    })

    this.optionsContainer = [];

    for (let index = 0; index < params.noOfColumns; index++) {
        const x = parseInt(index % params.noOfColumns)
        const settingColumn = scene.create({
            t: "rect",
            parent: this.container,
            id: index,
            h: settingViewH,
            w: params.columnWidth,
            y: params.gapV,
            x: (x * params.columnWidth) + (params.gapH * x) + 50,
            fillColor: 0x292929ff
        })
        this.optionsContainer.push(settingColumn)
    }

    const fontBold = scene.create({
        t: "fontResource", url: '/usr/share/fonts/DejaVuSans-Bold.ttf'
    });

    const getTextDimensions = (text) => {
        const textBox = scene.create({
            t: "textBox", font: fontBold,
            text, pixelSize: parent.w * 0.012
        });
        const { bounds } = textBox.measureText()
        const width = Math.abs(bounds.x1) + Math.abs(bounds.x2)
        const height = Math.abs(bounds.y1) + Math.abs(bounds.y2)
        return { width, height }
    }

    this.renderOptions = (columnIndex, reset) => {
        if (columnIndex >= params.noOfColumns) {
            return;
        }
        if (reset) {
            for (let index = 0; index < params.noOfColumns; index++) {
                this.optionsContainer[index].removeAll()
            }
        }
        this.optionsContainer[columnIndex].removeAll()

        switch (columnIndex) {
            case 0:
                if (optionsSetting) {
                    currentSetting = optionsSetting
                    idColumn = `menu`
                    idText = `name`
                    nextColumn = true
                } else {
                    nextColumn = false
                    return;
                }
                break
            case 1:
                if (optionsSetting[this.indexColumn[0]].children) {
                    currentSetting = optionsSetting[this.indexColumn[0]].children
                    idColumn = `menu${this.indexColumn[0]}`
                    idText = `name${this.indexColumn[0]}`
                    nextColumn = true
                } else {
                    nextColumn = false
                    return;
                }
                break
            case 2:
                if (optionsSetting[this.indexColumn[0]].children[this.indexColumn[1]].children) {
                    currentSetting = optionsSetting[this.indexColumn[0]].children[this.indexColumn[1]].children
                    idColumn = `menu${this.indexColumn[0]}${this.indexColumn[1]}`
                    idText = `name${this.indexColumn[0]}${this.indexColumn[1]}`
                    nextColumn = true
                } else {
                    nextColumn = false
                    return;
                }
                break
            case 3:
                if (optionsSetting[this.indexColumn[0]].children[this.indexColumn[1]].children[this.indexColumn[2]].children) {
                    currentSetting = optionsSetting[this.indexColumn[0]].children[this.indexColumn[1]].children[this.indexColumn[2]].children
                    idColumn = `menu${this.indexColumn[0]}${this.indexColumn[1]}${this.indexColumn[2]}`
                    idText = `name${this.indexColumn[0]}${this.indexColumn[1]}${this.indexColumn[2]}`
                    nextColumn = true
                } else {
                    nextColumn = false
                    return;
                }
                break
            default:
                currentSetting = optionsSetting
                idColumn = `menu`
                idText = `name`
                nextColumn = true
                break
        }
        const padding = 30

        let totalH = 0
        if (currentSetting && currentSetting.length > 0) {
            for (let index = 0; index < currentSetting.length; index++) {
                const option = currentSetting[index];
                const { width, height } = getTextDimensions(option.name)
                const prevParent = columnIndex > 0 ? this.optionsContainer[columnIndex-1].getObjectById(`${idColumn}`) : null
                const prevChild = this.optionsContainer[columnIndex].getObjectById(`${idColumn}${index - 1}`)
                let optionHolderY = 0;
                if (columnIndex > 0 && index === 0) {
                    optionHolderY = prevParent ? (prevParent.y) : 0
                } else {
                    optionHolderY = prevChild ? (prevChild.y + prevChild.h + params.gapH) : 0
                }
                const optionHolder = scene.create({
                    id: `${idColumn}${index}`,
                    x: 0,
                    y: optionHolderY,
                    parent: this.optionsContainer[columnIndex],
                    t: "rect", w: width + padding, h: optionHeight, fillColor: defaultColor
                });
                totalH = totalH + (height + padding + params.gapV)

                const centerY = (height + padding) / 2 - height / 2
                const centerX = (width + padding) / 2 - width / 2
                const textBox = scene.create({
                    id: `${idText}${index}`,
                    parent: optionHolder,
                    x: centerX,
                    y: centerY,
                    t: "textBox", font: fontBold,
                    text: option.name, pixelSize: parent.w * 0.012
                });
                // this.optionsInitials[index] = {
                //     containerW: width + padding,
                //     containerX: centerX,
                //     pixelSize: parent.w * 0.012,
                //     x: parent.w * 0.01,
                //     y: centerY,

                // }
            }
        }
        this.optionsContainer[columnIndex].h = totalH
        this.optionsContainer[columnIndex].y = 250

        this.optionsContainerW = this.optionsContainer[columnIndex].w
        this.optionsContainerY = this.optionsContainer[columnIndex].y
        this.optionsContainerH = this.optionsContainer[columnIndex].h
    }

    this.renderOptions(0, true);
    
    let options = {
        hostname: 'localhost',
        port: 10999,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    let AVStatusCallback = function (json) {
        for (let i = 0; i < json.paramList.length; i++) {
            if(avStatusObjectMap[json.paramList[i].name] === undefined)
                    continue;

            avStatusObjectMap[json.paramList[i].name] = json.paramList[i].value;
            console.log(json.paramList[i].name, json.paramList[i].value);
        }
        optionsSetting = [
            {
                name: "General",
                children: [
                    {
                        name: "Video",
                        children: [
                            {
                                name: "Aspect Ratio",
                                children: [
                                    {
                                        name: avStatusObjectMap["Device.Services.STBService.1.Components.VideoDecoder.1.ContentAspectRatio"]
                                    }
                                ]
                            },
                            {
                                name: "Resolution",
                                children: [
                                    {
                                        name: avStatusObjectMap["Device.Services.STBService.1.Components.HDMI.1.ResolutionValue"]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        name: "Audio Mode",
                        children: [
                            {
                                name: avStatusObjectMap["Device.Services.STBService.1.Components.AudioOutput.1.X_COMCAST-COM_AudioStereoMode"]
                            }
                        ]
                    },
                    {
                        name: "Help"
                    }
                ]
            },
            {
                name: "System",
                children: [
                    {
                        name: "Test System 1"
                    },
                    {
                        name: "Test System 2"
                    }
                ]
            }
        ]
    }

    let errorCallback = function (str) {

    }

    let postData = '{"paramList" : [ \
        {"name" : "Device.Services.STBService.1.Components.HDMI.1.ResolutionValue"}, \
        {"name" : "Device.Services.STBService.1.Components.VideoOutput.1.DisplayFormat"}, \
        {"name" : "Device.Services.STBService.1.Components.HDMI.1.DisplayDevice.SupportedResolutions"}, \
        {"name" : "Device.Services.STBService.1.Components.VideoDecoder.1.ContentAspectRatio}, \
        {"name" : "Device.Services.STBService.1.Components.AudioOutput.1.X_COMCAST-COM_AudioStereoMode"}, \
        {"name" : "Device.Services.STBService.1.Components.HDMI.1.DisplayDevice.PreferredResolution"}, \
        {"name" : "Device.Services.STBService.1.Components.VideoOutput.1.HDCP"}, \
        {"name" : "Device.DeviceInfo.X_RDKCENTRAL-COM_xBlueTooth.Enabled"}, \
        {"name" : "Device.DeviceInfo.X_RDKCENTRAL-COM_xBlueTooth.DeviceInfo.Profile"}, \
        {"name" : "Device.DeviceInfo.X_RDKCENTRAL-COM_xBlueTooth.DeviceInfo.Manufacturer"} \
    ]}';

    Utils.doHttpPost(options, postData).then(AVStatusCallback, errorCallback);

    this.setFocus = (focus) => {
        this.container.focus = focus
        if (focus) {
            this.setSelected(0, 0)
            this.indexColumn = [0, 0, 0, 0]
            this.renderOptions(1)
        } else {
            this.setSelected(0, 0, true)
        }
    }
    this.setSelected = (columnIndex, index, reset, resetColumn) => {
        let previousIdColumn = ""
        let previousIdText = ""
        switch (columnIndex) {
            case 0:
                previousSetting = optionsSetting
                idColumn = `menu`
                idText = `name`
                break
            case 1:
                previousSetting = optionsSetting[this.indexColumn[0]].children
                idColumn = `menu${this.indexColumn[0]}`
                idText = `name${this.indexColumn[0]}`
                break
            case 2:
                previousSetting = optionsSetting[this.indexColumn[0]].children[this.indexColumn[1]].children
                idColumn = `menu${this.indexColumn[0]}${this.indexColumn[1]}`
                idText = `name${this.indexColumn[0]}${this.indexColumn[1]}`
                break
            case 3:
                previousSetting = optionsSetting[this.indexColumn[0]].children[this.indexColumn[1]].children[this.indexColumn[2]].children
                idColumn = `menu${this.indexColumn[0]}${this.indexColumn[1]}${this.indexColumn[2]}`
                idText = `name${this.indexColumn[0]}${this.indexColumn[1]}${this.indexColumn[2]}`
                break
            default:
                previousSetting = optionsSetting
                idColumn = `menu`
                idText = `name`
                break
        }
        switch (this.previousColumnIndex) {
            case 0:
                previousIdColumn = `menu`
                previousIdText = `name`
                break
            case 1:
                previousIdColumn = `menu${this.indexColumn[0]}`
                previousIdText = `name${this.indexColumn[0]}`
                break
            case 2:
                previousIdColumn = `menu${this.indexColumn[0]}${this.indexColumn[1]}`
                previousIdText = `name${this.indexColumn[0]}${this.indexColumn[1]}`
                break
            case 3:
                previousIdColumn = `menu${this.indexColumn[0]}${this.indexColumn[1]}${this.indexColumn[2]}`
                previousIdText = `name${this.indexColumn[0]}${this.indexColumn[1]}${this.indexColumn[2]}`
                break
            default:
                previousIdColumn = `menu`
                previousIdText = `name`
                break
        }
        if (reset) {
            this.optionsContainer[columnIndex].getObjectById(`${idColumn}${this.previousIndex}`).fillColor = defaultColor
            this.optionsContainer[columnIndex].getObjectById(`${idText}${index}`).fillColor = defaultTextColor
            return
        }
        if (this.previousColumnIndex !== this.currentColumnIndex) {
            if (this.optionsContainer[this.previousColumnIndex].getObjectById(`${previousIdColumn}${this.previousIndex}`) && this.optionsContainer[this.previousColumnIndex].getObjectById(`${previousIdText}${this.previousIndex}`) && resetColumn) {
                this.optionsContainer[this.previousColumnIndex].getObjectById(`${previousIdColumn}${this.previousIndex}`).fillColor = defaultColor
                this.optionsContainer[this.previousColumnIndex].getObjectById(`${previousIdText}${this.previousIndex}`).textColor = defaultTextColor
            }
        } else {
            if (this.optionsContainer[columnIndex].getObjectById(`${idColumn}${this.previousIndex}`) && this.optionsContainer[columnIndex].getObjectById(`${idText}${this.previousIndex}`)) {
                this.optionsContainer[columnIndex].getObjectById(`${idColumn}${this.previousIndex}`).fillColor = defaultColor
                this.optionsContainer[columnIndex].getObjectById(`${idText}${this.previousIndex}`).textColor = defaultTextColor
            }
        }
        this.optionsContainer[columnIndex].getObjectById(`${idColumn}${index}`).fillColor = selectedColor
        this.optionsContainer[columnIndex].getObjectById(`${idText}${index}`).textColor = selectedTextColor
    }

    this.container.on("onKeyDown", (e) => {
        // if (e.keyCode === keys.ENTER) {
        //     params.onOptionSelect(options[this.currentIndex])
        // }
        this.previousIndex = this.indexColumn[this.currentColumnIndex]
        this.previousColumnIndex = this.currentColumnIndex
        if (e.keyCode === keys.RIGHT && this.currentColumnIndex < params.noOfColumns - 1) {
            if (nextColumn) {
                this.currentColumnIndex++
                this.indexColumn[this.currentColumnIndex] = 0;
                if (this.currentColumnIndex < params.noOfColumns - 1) {
                    this.renderOptions(this.currentColumnIndex + 1);
                }
                this.setSelected(this.currentColumnIndex, 0)
            } else {
                if (this.currentColumnIndex + 1 < params.noOfColumns - 1) {
                    this.renderOptions(this.currentColumnIndex + 2);
                }
            }
        }
        if (e.keyCode === keys.LEFT && this.currentColumnIndex > 0) {
            if (this.currentColumnIndex + 1 < params.noOfColumns) {
                this.optionsContainer[this.currentColumnIndex + 1].removeAll()
            }
            this.currentColumnIndex--
            this.setSelected(this.currentColumnIndex, this.indexColumn[this.currentColumnIndex], false, true)
        }

        if (e.keyCode === keys.UP) {
            if (this.currentColumnIndex === 0 && this.indexColumn[this.currentColumnIndex] === 0) {
                this.setFocus(false)
                this.resetAll()
                params.onBlur()
            } else {
                if (this.indexColumn[this.currentColumnIndex] > 0) {
                    this.indexColumn[this.currentColumnIndex]--
                    this.renderOptions(this.currentColumnIndex + 1)
                    this.setSelected(this.currentColumnIndex, this.indexColumn[this.currentColumnIndex])
                }
            }
        }

        if (e.keyCode === keys.DOWN) {
            if (this.indexColumn[this.currentColumnIndex] < previousSetting.length - 1) {
                this.indexColumn[this.currentColumnIndex]++
                this.renderOptions(this.currentColumnIndex + 1)
                this.setSelected(this.currentColumnIndex, this.indexColumn[this.currentColumnIndex])
            }
        }
    });

    this.resetAll = () => {
        this.renderOptions(0, true);
        this.indexColumn = [0, 0, 0, 0];
        idColumn = ""
        idText = ""

        this.currentIndex = 0;
        this.currentColumnIndex = 0;
        this.previousColumnIndex = 0;
        this.previousIndex = 0;
    }

    // this.updateSize = (changeW, changeH) => {
    //     this.container.h = containerH * changeH
    //     this.container.w = containerW * changeW
    //     this.container.x = containerX * changeW
    //     this.container.y = containerY * changeH

    //     this.imageBackground.h = imageBackgroundH * changeH
    //     this.imageBackground.w = imageBackgroundW * changeW

    //     this.image.h = imageH * changeH
    //     this.image.w = imageW * changeW
    //     this.image.x = (this.imageBackground.w - this.image.w) / 2
    //     this.image.y = (this.imageBackground.h - this.image.h) / 2

    //     if (this.status) {
    //         this.status.h = imageStatusH * changeH
    //         this.status.w = imageStatusW * changeW
    //         this.status.x = imageStatusX * changeW
    //         this.status.y = imageStatusY * changeH
    //     }

    //     this.text.h = textH * changeH
    //     this.text.w = textW * changeW
    //     this.text.pixelSize = textFontSize * changeH
    //     this.text.y = this.container.h - this.text.h
    // }
    // this.setStatus = (status) => {
    //     if (status === 'RUNNING') {
    //         this.status.url = `${directory}/images/app-play.svg`
    //     } else if (status === 'SUSPENDED') {
    //         this.status.url = `${directory}/images/app-pause.svg`
    //     } else {
    //         this.status.url = `${directory}/images/app-box.svg`
    //     }
    // }
}
module.exports = Settings;