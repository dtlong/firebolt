/** This version of rdkmediaplayer has been tested and works with RDK master as of April 4, 2018 */
var stuff = "https://px-apps.sys.comcast.net/pxscene-samples/examples/px-reference/frameworks/";
px.configImport({ "stuff:": stuff });
px.import({
    scene: 'px:scene.1.js',
    keys: 'px:tools.keys.js'
}).then(function importsAreReady(imports) {
    var showDebugUI = true;
    var handleRemote = true;
    let base = px.getPackageBaseFilePath();
    let baseImgUrl = base.replace("apps", "images");
    // var streams = [
    //     ["https://vtvgoimage.vtvdigital.vn/images/1__.png", "https://mnmedias.api.telequebec.tv/m3u8/29886.m3u8"],
    //     ["https://vtvgoimage.vtvdigital.vn/images/2__.png", "https://mnmedias.api.telequebec.tv/m3u8/29881.m3u8"],
    //     ["https://vtvgoimage.vtvdigital.vn/images/3__.png", "https://mnmedias.api.telequebec.tv/m3u8/29883.m3u8"],
    //     ["https://vtvgoimage.vtvdigital.vn/images/4__.png", "https://mnmedias.api.telequebec.tv/m3u8/29885.m3u8"],
    //     ["https://vtvgoimage.vtvdigital.vn/images/5__.png", "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"],
    //     ["https://vtvgoimage.vtvdigital.vn/images/6__.png", "https://mnmedias.api.telequebec.tv/m3u8/29887.m3u8"]
    // ];
    var streams = [
        [ baseImgUrl + "/stream_icon_1.jpg" , "https://mnmedias.api.telequebec.tv/m3u8/29886.m3u8"],
        [ baseImgUrl + "/stream_icon_2.jpg", "https://mnmedias.api.telequebec.tv/m3u8/29881.m3u8"],
        [ baseImgUrl + "/stream_icon_3.jpg", "https://mnmedias.api.telequebec.tv/m3u8/29883.m3u8"],
        [ baseImgUrl + "/stream_icon_4.jpg", "https://mnmedias.api.telequebec.tv/m3u8/29885.m3u8"],
        [ baseImgUrl + "/stream_icon_5.jpg", "https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"],
        [ baseImgUrl + "/stream_icon_6.png", "https://mnmedias.api.telequebec.tv/m3u8/29887.m3u8"]
    ]
    var scene = imports.scene;
    var keys = imports.keys;
    var splash = null;
    var controls = null;
    var logpanel = null;
    var waylandObj = null;
    var player = null;
    var autoTune = true;
    var audioLanguages = null;
    var currentSpeed = 1.0;
    var currentID = null;
    var isTuned = false;
    const playImgUrl = "/play.png";
    const pauseImgUrl = "/pause.png";
    const rewindImgUrl = "/rewind.png";
    const fastForwardUrl="/fast-forward.png";
    console.log(">>>>>>>>>>>>>>> browser base", baseImgUrl);
    module.exports.wantsClearscreen = function () {
        return false;
    };

    const event = scene.getService("getEventManager");
    // scene.addServiceProvider(function (serviceName, serviceCtx) {
    //     if (serviceName == "getEventManager")
    //         return event;
    //     else
    //         return "allow";
    // }.bind(this))
    event.on(event.ON_SELECT_STREAM, loadCurrentStream);
    event.on(event.ON_DESTROY_STREAM, function() {
        event.delListener(event.ON_SELECT_STREAM, loadCurrentStream);
    })

    function loadCurrentStream(index) {
        controls = new ControlPanel(index);
        if (showDebugUI)
            controls.background.moveToFront();
        if (streams.length > 0) {
            if (showDebugUI)
                loadStream();
            else
                doLoad(streams[0][index]);
        }
        setTimeout(hackToMakeEventsFlow1, 1000);
    }

    function log(msg) {
        console.log("rdkmediaplayer.js: " + msg);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //UI CONTROLS
    function centerText(textObj) {
        var size = textObj.font.measureText(textObj.pixelSize, textObj.text);
        textObj.x = (textObj.parent.w - size.w) / 2;
        textObj.y = (textObj.parent.h - size.h) / 2;
    }
    function ControlButton(inTb, inId, inX, inY, inW, inH, inAction, text, inURL) {
        this.tb = inTb;
        this.id = inId;
        this.focused = scene.create({
            t: "rect",
            x: inX,
            y: inY,
            w: inW,
            h: inH,
            parent: inTb.background,
            draw: false,
            fillColor: 0xFFFFFFFF
        });
        this.unfocused = scene.create({
            t: "rect",
            x: inX,
            y: inY,
            w: inW,
            h: inH,
            parent: inTb.background,
            draw: true,
            fillColor: 0x555555FF
        });
        this.focText = scene.create({
            t: "text",
            x: 0,
            y: 0,
            w: inW,
            h: inH,
            parent: this.focused,
            textColor: 0x000000FF,
            text: text
        });
        // this.focText = scene.create({
        //     t: "image9",
        //     x: 0,
        //     y: 0,
        //     w: inW,
        //     h: inH,
        //     url: text,
        //     focus: true,
        //     parent: this.focused
        // });
        this.focText.ready.then(centerText);
        this.unfocText = scene.create({
            t: "text",
            x: 0,
            y: 0,
            w: inW,
            h: inH,
            parent: this.unfocused,
            text: text
        });
        // this.unfocText = scene.create({
        //     t: "image9",
        //     x: 0,
        //     y: 0,
        //     w: inW,
        //     h: inH,
        //     url: text,
        //     focus: true,
        //     parent: this.unfocused
        // });
        this.unfocText.ready.then(centerText);
        this.isFocused = false;
        this.isVisible = true;
        this.left = null;
        this.right = null;
        this.action = inAction
        this.url = inURL;
    }
    function ControlButtonStream(inTb, inId, inX, inY, inW, inH, inAction, text, inURL) {
        this.tb = inTb;
        this.id = inId;
        this.focused = scene.create({
            t: "rect",
            x: inX,
            y: inY,
            w: inW,
            h: inH,
            parent: inTb.background,
            draw: false,
            fillColor: 0xFFFFFFFF
        });
        this.unfocused = scene.create({
            t: "rect",
            x: inX,
            y: inY,
            w: inW,
            h: inH,
            parent: inTb.background,
            draw: true,
            fillColor: 0x555555FF
        });
        // this.focText = scene.create({
        //     t: "text",
        //     x: 0,
        //     y: 0,
        //     w: inW,
        //     h: inH,
        //     parent: this.focused,
        //     textColor: 0x000000FF,
        //     text: text
        // });
        this.focText = scene.create({
            t: "image9",
            x: 2.5,
            y: 2.5,
            w: inW - 5,
            h: inH - 5,
            url: text,
            focus: true,
            parent: this.focused
        });
        this.focText.ready.then(centerText);
        // this.unfocText = scene.create({
        //     t: "text",
        //     x: 0,
        //     y: 0,
        //     w: inW,
        //     h: inH,
        //     parent: this.unfocused,
        //     text: text
        // });
        this.unfocText = scene.create({
            t: "image9",
            x: 2.5,
            y: 2.5,
            w: inW - 5,
            h: inH - 5,
            url: text,
            focus: true,
            parent: this.unfocused
        });
        this.unfocText.ready.then(centerText);
        this.isFocused = false;
        this.isVisible = true;
        this.left = null;
        this.right = null;
        this.action = inAction
        this.url = inURL;
    }
    ControlButton.prototype.updateState = function () {
        if (this.isVisible) {
            if (this.isFocused) {
                this.focused.draw = true;
                this.unfocused.draw = false;
            }
            else {
                this.focused.draw = false;
                this.unfocused.draw = true;
            }
        }
        else {
            this.focused.draw = false;
            this.unfocused.draw = false;
        }
    }
    ControlButton.prototype.setFocused = function (isFocused) {
        this.isFocused = isFocused
        this.updateState();
    }
    ControlButton.prototype.setVisible = function (isVisible) {
        this.isVisible = isVisible
        this.updateState();
    }
    ControlButtonStream.prototype.updateState = function () {
        if (this.isVisible) {
            if (this.isFocused) {
                this.focused.draw = true;
                this.unfocused.draw = false;
            }
            else {
                this.focused.draw = false;
                this.unfocused.draw = true;
            }
        }
        else {
            this.focused.draw = false;
            this.unfocused.draw = false;
        }
    }
    ControlButtonStream.prototype.setFocused = function (isFocused) {
        this.isFocused = isFocused
        this.updateState();
    }
    ControlButtonStream.prototype.setVisible = function (isVisible) {
        this.isVisible = isVisible
        this.updateState();
    }
    function ControlPanel(index = 0) {
        var PH = 180;//panel height
        var RH = PH / 3;//row height
        this.background = scene.create({
            t: "object",
            x: 0,
            y: scene.h - PH - 30,
            w: scene.w,
            h: PH,
            parent: scene.root
        });
        var W = 100;//button width
        var H = 30;//button height
        var S = 10;//space between button
        var X = scene.w / 2.0 - (W * 5) - (S * 4.5);
        var Y = RH * 0.5 - H / 2;
        var D = W + S;
        // this.divider0 = scene.create({
        //     t: "rect",
        //     x: 0,
        //     y: 0,
        //     w: scene.w,
        //     h: 2,
        //     parent: this.background
        // });
        // this.title0 = scene.create({
        //     t: "text",
        //     x: 10,
        //     y: 5,
        //     parent: this.background,
        //     pixelSize: 14,
        //     text: "Info"
        // });
        // this.streamName = scene.create({
        //     t: "text",
        //     x: X,
        //     y: Y,
        //     parent: this.background,
        //     text: ""
        // });
        // this.streamPosition = scene.create({
        //     t: "text",
        //     x: X + 300,
        //     y: Y,
        //     parent: this.background,
        //     text: ""
        // });
        this.line = scene.create({
            t: "image9",
            url: baseImgUrl + "/horizontal-line.png",
            x: X + 400,
            y: Y,
            w: 300,
            h: 80,
            parent: this.background,
        });
        this.ball = scene.create({
            t: "image9",
            x: X + 400,
            y: Y,
            w: 10,
            h: 80,
            url: baseImgUrl + "/horizontal-ball.png", a: 1,
            parent: this.background
        });
        Y = RH * 1.5 - H / 2;
        // this.divider1 = scene.create({
        //     t: "rect",
        //     x: 10,
        //     y: RH,
        //     w: scene.w - 20,
        //     h: 2,
        //     parent: this.background
        // });
        // this.title1 = scene.create({
        //     t: "text",
        //     x: 10,
        //     y: RH + 5,
        //     parent: this.background,
        //     pixelSize: 14,
        //     text: "Controls"
        // });
        this.buttons = [
            new ControlButtonStream(this, 0, X + D * 0 + 400, Y, 30, 30, doSeekBk, baseImgUrl + rewindImgUrl, null),
            new ControlButtonStream(this, 1, X + 120 + 400, Y, 30, 30, doPlay, baseImgUrl + playImgUrl, null),
            new ControlButtonStream(this, 2, X + 170 + 400, Y, 30, 30, doPause,baseImgUrl + pauseImgUrl, null),
            new ControlButtonStream(this, 3, X + 260 + 400, Y, 30, 30, doSeekFr, baseImgUrl + fastForwardUrl, null),
            // new ControlButton(this, 4, X + D * 3, Y, W, H, doLive, "LIVE", null),
            // new ControlButton(this, 5, X + D * 4, Y, W, H, doREW, "REW", null),
            // new ControlButton(this, 6, X + D * 5, Y, W, H, doFFW, "FFW", null),
            // new ControlButton(this, 7, X + D * 6, Y, W, H, doSeekBk, "SEEK-", null),
            // new ControlButton(this, 8, X + D * 7, Y, W, H, doSeekFr, "SEEK+", null),
            // new ControlButton(this, 9, X + D * 8, Y, W, H, doCC, "CC", null),
            // new ControlButton(this, 10, X + D * 9, Y, W, H, doSap, "SAP", null),
        ];
        var len = this.buttons.length;
        for (i = 0; i < len - 1; i++) {
            this.buttons[i].right = this.buttons[i + 1];
            this.buttons[i + 1].left = this.buttons[i];
        }
        this.buttons[0].left = this.buttons[len - 1];
        this.buttons[len - 1].right = this.buttons[0];
        Y = RH * 2.5 - H / 2;
        this.channels = [];
        for (i = 0; i < streams.length; i++) {
            this.channels.push(new ControlButtonStream(this, 10 + i, X + D * i + 250, Y, 55, 55, loadStream, streams[i][0], streams[i][1]));
        }
        len = this.channels.length;
        for (i = 0; i < len - 1; i++) {
            this.channels[i].right = this.channels[i + 1];
            this.channels[i + 1].left = this.channels[i];
        }
        if (len > 0) {
            this.channels[0].left = this.channels[len - 1];
            this.channels[len - 1].right = this.channels[0];
        }
        // this.divider2 = scene.create({
        //     t: "rect",
        //     x: 10,
        //     y: RH * 2,
        //     w: scene.w - 20,
        //     h: 2,
        //     parent: this.background
        // });
        // this.title2 = scene.create({
        //     t: "text",
        //     x: 10,
        //     y: RH * 2 + 5,
        //     parent: this.background,
        //     pixelSize: 14,
        //     text: "Streams"
        // });
        currentSpeed = 1.0;
        this.supportedSpeeds = [-64.0, -32.0, -16.0, -4.0, -1.0, 0.0, 1.0, 4.0, 16.0, 32.0, 64.0];
        this.focused = true;
        this.focusedButton = this.channels[index];
        this.updateState();
    }
    ControlPanel.prototype.updateState = function () {
        this.focusedButton.setFocused(true);
        this.background.draw = this.focused;
        logpanel.setVisible(this.focused);
    }
    ControlPanel.prototype.navLeft = function () {
        if (this.focusedButton.left) {
            this.focusedButton.setFocused(false);
            this.focusedButton = this.focusedButton.left;
            this.focusedButton.setFocused(true);
        }
    }
    ControlPanel.prototype.navRight = function () {
        if (this.focusedButton.right) {
            this.focusedButton.setFocused(false);
            this.focusedButton = this.focusedButton.right;
            this.focusedButton.setFocused(true);
        }
    }
    ControlPanel.prototype.navUp = function () {
        if(this.focusedButton.id > 3) {
            this.focusedButton.setFocused(false);
            this.focusedButton = this.buttons[3];
            this.focusedButton.setFocused(true);
        } else {
            this.focused = false;
            this.background.draw = false;
            logpanel.setVisible(false);
        }
    }
    ControlPanel.prototype.navDown = function () {
        if (!this.focused) {
            controls.focused = true;
            controls.background.draw = true;
            logpanel.setVisible(true);
        }
        else if (this.focusedButton.id < 4 && this.channels.length > 0) {
            this.focusedButton.setFocused(false);
            if (this.focusedButton.id > this.channels.length - 1)
                this.focusedButton = this.channels[this.channels.length - 1];
            else
                this.focusedButton = this.channels[this.focusedButton.id];
            this.focusedButton.setFocused(true);
        }
    }
    ControlPanel.prototype.doAction = function () {
        this.focusedButton.action.call();
    }
    ControlPanel.prototype.loadStream = function () {
        log("loadStream");
        var id = this.focusedButton.id;
        if (id >= 10)
            id -= 10;
        else
            id = 0;
        // this.streamName.text = this.channels[id].focText.text + ": " + this.channels[id].url;

        // var nameSize = this.streamName.font.measureText(this.streamName.pixelSize, this.streamName.text);
        // this.streamPosition.x = this.streamName.x + nameSize.w + 50;
        // this.streamPosition.text = "Position 0 of 0";
        this.ball.w = 10;
        logpanel.clear();
        splash.setVisible(true);
        currentID = this.channels[id];
        doLoad(this.channels[id].url);
    }
    ControlPanel.prototype.updateTime = function (position, duration) {
        //log("updateTime: " + t);
        // this.streamPosition.text = "Position " + position.toFixed(2) + " of " + duration.toFixed(2);
        var loadPercent = (position.toFixed(2))/ (duration.toFixed(2));
        var speed = (Math.random()*5)+0.3;
        // this.ball.animateTo({ x: 500 }, duration, scene.animation.LINEAR,scene.animation.OPTION_LOOP, scene.animation.COUNT_FOREVER);
        // this.ball.animateTo({ x: 500 }, 2, scene.animation.LINEAR,scene.animation.OPTION_FASTFORWARD, scene.animation.COUNT_FOREVER);
        if(this.ball.w < this.line.w) {
            this.ball.w = loadPercent*(this.line.w);
        }
    }
    function loadStream() {
        controls.loadStream();
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    //LOG PANEL
    function LogPanel() {
        var PW = 200; //panel width
        this.background = scene.create({
            t: "rect",
            x: scene.w - PW,
            y: 0,
            w: PW,
            h: 0,
            parent: scene.root,
            fillColor: 0x000000AA
        });
        this.textbox = scene.create({
            t: "textBox",
            x: 10,
            y: 0,
            w: this.background.w - 10,
            h: 720,
            parent: this.background,
            pixelSize: 12,
            wordWrap: true,
            truncation: scene.truncation.TRUNCATE_AT_WORD,
            ellipsis: true
        });
    }
    LogPanel.prototype.setVisible = function (visible) {
        this.background.draw = visible;
    }
    LogPanel.prototype.clear = function () {
        this.textbox.text = "";
    }
    LogPanel.prototype.log = function (message) {
        log(message);
        this.textbox.text = this.textbox.text + message + "\n";
        var size = this.textbox.font.measureText(this.textbox.pixelSize, this.textbox.text);
        this.background.h = size.h;
        //do some rudimentary scrolling
        while (this.background.h > controls.background.y) {
            var text = this.textbox.text;
            var i = text.indexOf("\n");
            if (i >= 0) {
                text = text.substr(i + 1, text.length - i + 1);
                this.textbox.text = text;
                var size = this.textbox.font.measureText(this.textbox.pixelSize, this.textbox.text);
                this.background.h = size.h;
            }
            else
                break;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    //PLAYER STUFF
    function onEvent(e) {
        if (showDebugUI)
            logpanel.log("Event " + e.name);
    }
    function onMediaOpened(e) {
        if (showDebugUI)
            logpanel.log("Event " + e.name
                + "\n   type:" + e.mediaType
                + "\n   width:" + e.width
                + "\n   height:" + e.height
                + "\n   speed:" + e.availableSpeeds
                + "\n   sap:" + e.availableAudioLanguages
                + "\n   cc:" + e.availableClosedCaptionsLanguages
                //+ "\n   customProperties:" + e.customProperties
                //+ "\n   mediaSegments:" + e.mediaSegments
            );
        audioLanguages = e.availableAudioLanguages.split(",");
    }
    function onProgress(e) {
        if (showDebugUI) {

            logpanel.log("Event " + e.name
                + "\n   position:" + e.position
                + "\n   duration:" + e.duration
                + "\n   speed:" + e.speed
                + "\n   start:" + e.start
                + "\n   end:" + e.end
            );

            if (!isTuned) {
                isTuned = true;
                splash.setVisible(false);
                logpanel.setVisible(false);
                logpanel.log("Tune Complete");
            }
            controls.updateTime(e.position, e.duration);
        }
    }
    function onStatus(e) {
        if (showDebugUI)
            logpanel.log("Event " + e.name
                + "\n   position:" + e.position
                + "\n   duration:" + e.duration
                + "\n   width:" + e.width
                + "\n   height:" + e.height
                + "\n   isLive:" + e.isLive
                + "\n   isBuffering:" + e.isBuffering
                + "\n   connectionURL:" + e.connectionURL
                + "\n   bufferPercentage:" + e.bufferPercentage
                + "\n   dynamicProps:" + e.dynamicProps
                + "\n   netStreamInfo:" + e.netStreamInfo
            );
    }
    function onWarningOrError(e) {
        if (showDebugUI)
            logpanel.log("Event " + e.name + "\n   code:" + e.code + "\n   desc:" + e.description);
    }
    function onSpeedChange(e) {
        if (showDebugUI)
            logpanel.log("Event " + e.name + "\n   speed:" + e.speed);
    }
    function onComplete() {
        // splash.text = scene.create({
        //     t: "text",
        //     x: scene.w / 2 - 100,
        //     y: scene.h / 2 - 20,
        //     w: 0,
        //     h: 0,
        //     parent: this.background,
        //     pixelSize: 50,
        //     text: ""
        // });
        splash.text.text="Stream finished";
        splash.setVisible(true);
    }
    function registerPlayerEvents() {
        player.on("onMediaOpened", onMediaOpened);
        player.on("onProgress", onProgress);
        player.on("onStatus", onStatus);
        player.on("onWarning", onWarningOrError);
        player.on("onError", onWarningOrError);
        player.on("onSpeedChange", onSpeedChange);
        player.on("onClosed", onEvent);
        player.on("onPlayerInitialized", onEvent);
        player.on("onBuffering", onEvent);
        player.on("onPlaying", onEvent);
        player.on("onPaused", onEvent);
        player.on("onComplete", onComplete);
        player.on("onIndividualizing", onEvent);
        player.on("onAcquiringLicense", onEvent);
        player.on("onDRMMetadata", onEvent);
        player.on("onSegmentStarted", onEvent);
        player.on("onSegmentCompleted", onEvent);
        player.on("onSegmentWatched", onEvent);
        player.on("onBufferWarning", onEvent);
        player.on("onPlaybackSpeedsChanged", onEvent);
        player.on("onAdditionalAuthRequired", onEvent);
    }
    function unRegisterPlayerEvents() {
        player.delListener("onMediaOpened", onMediaOpened);
        player.delListener("onProgress", onProgress);
        player.delListener("onStatus", onStatus);
        player.delListener("onWarning", onWarningOrError);
        player.delListener("onError", onWarningOrError);
        player.delListener("onSpeedChange", onSpeedChange);
        player.delListener("onClosed", onEvent);
        player.delListener("onPlayerInitialized", onEvent);
        player.delListener("onBuffering", onEvent);
        player.delListener("onPlaying", onEvent);
        player.delListener("onPaused", onEvent);
        player.delListener("onComplete", onComplete);
        player.delListener("onIndividualizing", onEvent);
        player.delListener("onAcquiringLicense", onEvent);
        player.delListener("onDRMMetadata", onEvent);
        player.delListener("onSegmentStarted", onEvent);
        player.delListener("onSegmentCompleted", onEvent);
        player.delListener("onSegmentWatched", onEvent);
        player.delListener("onBufferWarning", onEvent);
        player.delListener("onPlaybackSpeedsChanged", onEvent);
        player.delListener("onAdditionalAuthRequired", onEvent);
    }
    function hackToMakeEventsFlow1() {
        setInterval(hackToMakeEventsFlow2, 250);
    }
    function hackToMakeEventsFlow2() {
        player.volume;
    }
    function doLoad(url) {
        log("doLoad");
        isTuned = false;
        player.url = url;
        player.setVideoRectangle(0, 0, scene.w, scene.h);
    }
    function doPause() {
        log("doPause");
        currentSpeed = 0.0;
        player.pause();
        if (showDebugUI)
            controls.updateState();
    }
    function doPlay() {
        log("doPlay");
        currentSpeed = 1.0;
        player.play();
        if (showDebugUI)
            controls.updateState();
    }
    function doPlayPrevious() {
        log("doPlayPrevious");
        currentSpeed = 1.0;
        if (currentID > 0) {
            player.url = this.channels[currentID - 1].url;
            player.setVideoRectangle(0, 0, scene.w, scene.h);
        }
        player.play();
        if (showDebugUI)
            controls.updateState();
    }
    function doFFW() {
        log("doFFW");
        if (currentSpeed < 0)
            currentSpeed = 4;
        else
            if (currentSpeed == 0) {
                currentSpeed = 1;
                player.play();
                return;
            }
            else
                if (currentSpeed == 1)
                    currentSpeed = 4;
                else
                    if (currentSpeed == 4)
                        currentSpeed = 16;
                    else
                        if (currentSpeed == 16)
                            currentSpeed = 32;
                        else
                            if (currentSpeed == 32)
                                currentSpeed = 64;
                            else
                                return;
        player.speed = currentSpeed;
        if (showDebugUI)
            controls.updateState();
    }
    function doREW() {
        log("doREW");
        if (currentSpeed > -4)
            currentSpeed = -4;
        else
            if (currentSpeed == -4)
                currentSpeed = -16;
            else
                if (currentSpeed == -16)
                    currentSpeed = -32;
                else
                    if (currentSpeed == -32)
                        currentSpeed = -64;
                    else
                        return;
        player.speed = currentSpeed;
        if (showDebugUI)
            controls.updateState();
    }
    function doRestart() {
        log("doRestart");
        player.position = 0;
        if (showDebugUI)
            controls.updateState();
    }
    function doLive() {
        log("doLive");
        player.seekToLive();
        if (showDebugUI)
            controls.updateState();
    }
    function doSeekFr() {
        log("doSeekFr");
        player.position = player.position + 60000.0;
    }
    function doSeekBk() {
        log("doSeekBk");
        player.position = player.position - 60000.0;
    }
    function doSap() {
        log("doSap");
        if (audioLanguages == null || audioLanguages.length < 2)
            return;
        if (player.audioLanguage == audioLanguages[0])
            player.audioLanguage = audioLanguages[1];
        else
            player.audioLanguage = audioLanguages[0];
        if (showDebugUI)
            controls.updateState();
    }
    function doCC() {
        log("doCC");
        if (player.closedCaptionsEnabled == "true")
            player.closedCaptionsEnabled = "false";
        else
            player.closedCaptionsEnabled = "true";
        if (showDebugUI)
            controls.updateState();
    }
    function handleWaylandRemoteSuccess(wayland) {
        log("Handle wayland success");
        waylandObj = wayland;
        waylandObj.moveToBack();
        player = wayland.api;
        registerPlayerEvents();
        /*
            player.closedCaptionsOptions = { 
                textSize:             "textSizeVal",
                fontStyle:            "fontStyleVal",
                textForegroundColor:  "textForegroundColorVal",
                textForegroundOpacity:"textForegroundOpacityVal",
                textBackgroundColor:  "textBackgroundColorVal",
                textBackgroundOpacity:"textBackgroundOpacityVal",
                textItalicized:       "textItalicizedVal",
                textUnderline:        "textUnderlineVal",
                windowFillColor:      "windowFillColorVal",
                windowFillOpacity:    "windowFillOpacityVal",
                windowBorderEdgeColor:"windowBorderEdgeColorVal",
                windowBorderEdgeStyle:"windowBorderEdgeStyleVal",
                textEdgeColor:        "textEdgeColorVal",
                textEdgeStyle:        "textEdgeStyleVal",
                fontSize:             "fontSizeVal" 
                };
        */
        // if (streams.length > 0) {
        //     if (showDebugUI)
        //         loadStream();
        //     else
        //         doLoad(streams[0][1]);
        // }
        // setTimeout(hackToMakeEventsFlow1, 1000);
    }
    function handleWaylandRemoteError(error) {
        log("Handle wayland error");
        //delete process.env.PLAYERSINKBIN_USE_WESTEROSSINK;
        if (showDebugUI)
            splash.setVisible(false);
    }
    if (showDebugUI || handleRemote) {
        scene.root.on("onKeyDown", function (e) {
            var code = e.keyCode;
            var flags = e.flags;
            log("onKeyDown " + code);
            if (showDebugUI) {
                if (code == keys.ENTER || code == 0) {
                    if (controls.focused) {
                        if (controls.focusedButton.id >= 10) {
                            splash.text.text = "Loading";
                        }
                        controls.doAction();
                    }
                }
                else if (code == keys.UP) {
                    if (controls.focused)
                        controls.navUp();
                }
                else if (code == keys.DOWN) {
                    controls.navDown();
                }
                else if (code == keys.LEFT) {
                    if (controls.focused)
                        controls.navLeft();
                }
                else if (code == keys.RIGHT) {
                    if (controls.focused)
                        controls.navRight();
                }
            }
            if (handleRemote) {
                if (code == keys.F) {
                    doFFW();
                }
                else if (code == keys.W) {
                    doREW();
                }
                else if (code == keys.P) {
                    if (currentSpeed == 1)
                        doPause();
                    else
                        doPlay();
                }
                else if (code == keys.PAGEUP) {
                    doSeekFr();
                }
                else if (code == keys.PAGEDOWN) {
                    doSeekBk();
                }
            }
        });
    }
    function SplashScreen() {
        this.background = scene.create({
            t: "rect",
            x: 0,
            y: 0,
            w: scene.w,
            h: scene.h,
            parent: scene.root,
            fillColor: 0x000000FF
        });
        this.text = scene.create({
            t: "text",
            x: scene.w / 2 - 100,
            y: scene.h / 2 - 20,
            w: 0,
            h: 0,
            parent: this.background,
            pixelSize: 50,
            text: "Loading"
        });
        this.text.ready.then(centerText);
    }
    SplashScreen.prototype.setVisible = function (visible) {
        this.background.draw = visible;
        this.text.draw = visible;
    }
    if (showDebugUI) {
        splash = new SplashScreen();
        logpanel = new LogPanel();
        // controls = new ControlPanel();
    }
    if (autoTune) {
        waylandObj = scene.create({ t: "wayland", x: 0, y: 0, w: 1920, h: 1080, parent: scene.root, cmd: "rdkmediaplayer" });
        waylandObj.remoteReady.then(handleWaylandRemoteSuccess, handleWaylandRemoteError);
        waylandObj.focus = true;
        waylandObj.moveToBack();
    }
    else {
        if (showDebugUI)
            splash.setVisible(false);
            controls.setVisible(false);
            logpanel.setVisible(false);
    }
    // if (showDebugUI)
    //     controls.background.moveToFront();
}).catch(function importFailed(err) {
    console.error("Import failed for rdkmediaplayer.js: " + err)
});